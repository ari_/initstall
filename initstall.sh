#!/bin/bash

cat << EndOfMessage

##########################################################
#                                                        #
#  o/ fapperz,                                           #
#  initstall.sh will configure your system.              #
#  Mofify the script as you want, I'm not your momz.     #
#                                                        #
#  Usage :                                               #
#  # ./initstall.sh [-h] [user] [port_ssh] [ufw]         #
#  Rulz                                                  #
#                                                        #
##########################################################

EndOfMessage


# you need to be root for exec this script
if [ "$UID" -ne 0 ]
then
        echo "[o] You need to be root"
        exit
fi

if [ -n "$1" ]
then
        user=$1
        echo "[+] User : $user"
        user_present=true
fi


echo "[+] Set timezone to local time in France"
timedatectl set-timezone Europe/Paris

echo "[+] Update & upgrade packages"
apt-get -qq update && apt-get -qq upgrade

echo "[+] Installation of usual packages and tools"
apt-get -qq install -qq -y sudo bash-completion build-essential python-pip curl checkinstall git libssl-dev nmap openssh-server vim p7zip-full tor proxychains ufw > /dev/null

# Conf ssh port if set in args
if [ -n "$2" ]
then
        echo "[+] Set ssh port to $2"
        sed -i 's/Port 22/Port $2/g' /etc/ssh/sshd_config
fi


service ssh restart

# if user, Add user to sudo group
if [ "$user_present" = true ]
then
        echo "[+] Add $user to sudo group"
        sudo adduser $user sudo
        cd /home/$user
fi

echo "[+] Creation of .bash_alases with basics shortcuts"
touch .bash_aliases

cat > .bash_aliases <<- EOM

#################################################
#                                               #
#               ./bash_aliases                  #
#                                               #
#################################################

alias newalias='cd ~ && sudo nano .bash_aliases'
alias upalias='source ~/.bashrc'

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'

alias lsa='ls -larth'
alias lll='ls -laRSC'
alias ali='alias | grep $1'

alias historyx='fc -l -800'

alias pse='ps -edf | grep -i $1'
alias psa='ps aux | grep -i $1'
alias kila='sudo kill -9 $1'
alias procactif='sudo lsof -i'

alias ipl="ifconfig eth0 | grep 'inet adr' | cut -f2 -d':' | cut -f1 -d' '"
alias ipr='sudo curl ifconfig.io'
alias ipt='sudo proxychains curl ifconfig.io'
alias ipta='sudo iptables -L -t nat --line-number'

alias ram='free -th'
alias mem='df -h'

EOM

source ~/.bashrc
cd ~


# Create Banner (like pi)
echo [+] Banner creation
cat > /etc/motd.sh <<- EOM
#!/bin/bash

echo ""
echo "  o/ $(whoami), Welcome on $(uname -n)"
echo ""
echo "  Last Login....: $(lastlog | grep $(whoami) | cut -d '.' -f 2,3)"
echo "  Uptime........: $(uptime | cut -d ',' -f 1)"
echo "  Load..........: $(uptime | cut -d ':' -f 5)"
echo "  Memory Usage..:  total : $(free -h | grep 'Mem' | awk '{print $2}') / used : $(free -h | grep 'Mem' | awk '{print $3}') / free $(free -h | grep 'Mem' | awk '{print $4}')"
echo "  Disk Usage....:  total : $(df -h | grep '/dev/sd' | awk '{print $2}') / used : $(df -h | grep '/dev/sd' | awk '{print $3}') / free : $(df -h | grep '/dev/sd' | awk '{print $4}')"
echo "  SSH Logins....:$(uptime | cut -d ',' -f 3)"
echo ""

EOM

sed -i '/uname/d' /etc/init.d/motd
sed -i "s/PrintLastLog yes/PrintLastLog no/g" /etc/ssh/sshd_config
chmod +x /etc/motd.sh
echo "/etc/motd.sh" >> /etc/profile



# Firewall ufw configuration
if [ -n "$3" ]
then
        echo "[+] ufw configuration, denies all inbound and allow ssh"
        ufw default deny incoming > /dev/null
        ufw default allow outgoing > /dev/null
        ufw allow $2 > /dev/null
        sed -i.bak 's/ENABLED=no/ENABLED=yes/g' /etc/ufw/ufw.conf
        chmod 0644 /etc/ufw/ufw.conf
fi


# Finish
echo "[+] System is ready !"
echo ""
echo "\o"
echo ""
